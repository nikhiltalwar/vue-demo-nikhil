**Demo Vue App**


## How to run



1. Git Clone
2. npm install
3. npm run dev


---

## Areas Covered

1. Two way data binding
2. Conditionals and loops v-for
3. Vue CLI
4. Life Cycle Hooks
5. Event Bus
6. Slots
7. Forms
8. HTTP GET & POST
9. Router & Router Parameters
